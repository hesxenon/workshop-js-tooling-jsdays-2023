# Requirements

- node (some recent version should be fine)
- [ticker.js](https://web.archive.org/web/19990203103422fw_/http://planetx.bloomu.edu/~mpscho/jsarchive/ticker.html)
- [clock.js](https://web.archive.org/web/19981205192251fw_/http://planetx.bloomu.edu/~mpscho/jsarchive/clock.html)
