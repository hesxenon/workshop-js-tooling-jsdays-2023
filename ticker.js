// Michael P. Scholtis (mpscho@planetx.bloomu.edu)
// All rights reserved.  January 19, 1996
// You may use this JavaScript example as you see fit, as long as the
// information within this comment above is included in your script.

var timerID = null;
var timerRunning = false;
var id,
  pause = 0,
  position = 0;

export function createTicker(headline) {
  var form = document.createElement("form");
  var tickerElement = document.createElement("input");
  form.appendChild(tickerElement);
  document.body.appendChild(form);

  ticker(headline, tickerElement);
}

function ticker(headline, tickerElement) {
  var i,
    k,
    msg = headline;
  k = 75 / msg.length + 1;
  for (i = 0; i <= k; i++) msg += " " + msg;

  tickerElement.value = msg.substring(position, position + 75);
  if (position++ == 38) position = 0;
  id = setTimeout(function () {
    ticker(headline, tickerElement);
  }, 1000 / 10);
}

function action() {
  if (!pause) {
    clearTimeout(id);
    pause = 1;
  } else {
    ticker();
    pause = 0;
  }
}
