const path = require("path");

const externalHeadlines = path.resolve(process.cwd(), "./headlines.js");

module.exports = {
  input: "./index.js",
  external: [externalHeadlines],
  output: {
    format: "iife",
    globals: {
      [externalHeadlines]: "headlines",
    },
    file: "./bundle/index.js",
  },
  plugins: [
    {
      name: "file url loader",
      transform: (code, id) => {
        const extension = path.extname(id);
        if (!(extension === ".jpg" || extension === ".png")) {
          return null;
        }
        return {
          code: `export default "${path.relative(process.cwd(), id)}"`,
        };
      },
    },
  ],
};
