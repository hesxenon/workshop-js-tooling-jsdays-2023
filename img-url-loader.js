const path = require("path");

module.exports = function () {
  return `module.exports = '${path.relative(process.cwd(), this.resourcePath)}'`;
};

