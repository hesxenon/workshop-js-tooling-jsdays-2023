const path = require("path");

module.exports = {
  mode: "development",
  entry: {
    index: "./index.js",
  },
  output: {
    path: path.join(process.cwd(), "./bundle"),
  },
  externals: {
    "./headlines": "headlines",
  },
  module: {
    rules: [
      {
        test: /\.jpg$/,
        loader: "./img-url-loader.js"
      }
    ]
  }
};
