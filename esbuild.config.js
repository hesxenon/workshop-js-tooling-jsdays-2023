const esbuild = require("esbuild");
const path = require("path");

esbuild.build({
  entryPoints: {
    index: "./index.js",
  },
  outdir: "./bundle",
  bundle: true,
  external: ["./headlines"],
  plugins: [
    {
      name: "img url file loader",
      setup: (build) => {
        build.onLoad({ filter: /\.jpg/ }, (args) => {
          return {
            contents: `export default "${path.relative(
              process.cwd(),
              args.path,
            )}"`,
          };
        });
      },
    },
    {
      name: "externalize headlines",
      setup: (build) => {
        build.onLoad({ filter: /headlines/ }, () => {
          return {
            contents: "export default headlines",
            loader: "js",
          };
        });
      },
    },
  ],
});
