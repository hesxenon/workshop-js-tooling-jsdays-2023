import headlines from "./headlines";
import { createTicker } from "./ticker";
import { startclock } from "./clock";
import imgSrc from "./banner.jpg";

function init() {
  var img = document.createElement("img");
  img.setAttribute("src", imgSrc);
  img.setAttribute("style", "width:100vw;height:10rem;object-fit:cover");

  document.body.appendChild(img);

  headlines.forEach((headline) => createTicker(headline));
  startclock();
}

init();
